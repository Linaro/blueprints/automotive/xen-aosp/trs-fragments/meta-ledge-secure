# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRCREV = "159729501c8c48d280978a86f3b580ae0dd9737d"

XEN_BRANCH = "upstream_maintenance_20230718"

SRC_URI = " \
    git://gitlab.com/Linaro/blueprints/automotive/xen-aosp/xen.git;branch=${XEN_BRANCH};protocol=https \
    file://0001-menuconfig-mconf-cfg-Allow-specification-of-ncurses-location.patch \
    file://0001-Fixup-smmu-v3.patch \
    file://0001-arm-acpi-don-t-expose-the-ACPI-IORT-SMMUv3-entry-to-.patch \
    file://acpi.cfg \
    file://msi.cfg \
    file://ioreq.cfg \
    file://xen.cfg.in"

SRC_URI += "file://uefi-certificates/db.key"
SRC_URI += "file://uefi-certificates/db.crt"

DEPENDS:append = " gettext-native e2fsprogs-native  efitools-native  coreutils-native "

DOM0_MEMORY_SIZE ??= "4096"

inherit image-efi-boot

do_compile:append:aarch64() {
        ${STAGING_BINDIR_NATIVE}/sbsign \
                --key ${WORKDIR}/uefi-certificates/db.key \
                --cert ${WORKDIR}/uefi-certificates/db.crt ${B}/xen/xen.efi \
                --output ${B}/xen/xen.efi.signed
}

do_deploy:append() {

    export DOM0_MEMORY_SIZE="${DOM0_MEMORY_SIZE}M"
    export ROOT_FS_UUID="${ROOT_FS_UUID}"
    export MACHINE="${MACHINE}"

    envsubst < ${WORKDIR}/xen.cfg.in > ${DEPLOYDIR}/xen.cfg

    if [ -f ${B}/xen/xen.efi.signed ]; then
        install -m 0644 ${B}/xen/xen.efi.signed ${DEPLOYDIR}/xen-${MACHINE}.efi.signed
    fi
}
